package com.example.demo.services;

import com.example.demo.dto.ProductDto;
import com.example.demo.entities.Product;
import com.example.demo.repository.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

@Service
public class ProductService {

    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    private ProductRepository productRepository;


    public List<ProductDto> getAllProducts(){
        return productRepository.findAll().stream()
                .map(p -> modelMapper.map(p,ProductDto.class))
                .toList();
    }

    public ProductDto create(ProductDto productDTO){
        Product product = modelMapper.map(productDTO, Product.class);
        return modelMapper.map(productRepository.save(product), ProductDto.class);
    }

    public void deleteById(Long id){
        productRepository.deleteById(id);
    }

    public ProductDto updateProduct(ProductDto productDTO, Long id) {
        Product existingProduct = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product not found"));


        Product request = modelMapper.map(productDTO, Product.class);
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(request, existingProduct);
        productRepository.save(request);

        return productDTO;
    }}
