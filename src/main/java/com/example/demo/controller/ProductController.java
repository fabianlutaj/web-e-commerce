package com.example.demo.controller;

import com.example.demo.dto.ProductDto;
import com.example.demo.entities.Product;
import com.example.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/all")
    public List<ProductDto> getAllProducts(){
        productService.getAllProducts();
        return  productService.getAllProducts();}


    @PostMapping("/create")
    public ResponseEntity<ProductDto> create(@RequestBody ProductDto productDTO){
        productService.create(productDTO);
        return new ResponseEntity<>(productDTO, HttpStatusCode.valueOf(200));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<ProductDto> delete(@RequestBody ProductDto productDTO, Long id){

        productService.deleteById(id);

        return new ResponseEntity<>(productDTO,HttpStatusCode.valueOf(200));
    }
    @PatchMapping("/{id}")
    public ResponseEntity<ProductDto> updateProduct (@RequestBody ProductDto productDto, @PathVariable Long id){
        productService.updateProduct(productDto,id);

        return new ResponseEntity<>(productDto,HttpStatusCode.valueOf(200));
    }


}
